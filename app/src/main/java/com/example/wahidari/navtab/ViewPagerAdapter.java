package com.example.wahidari.navtab;

/**
 * Created by Wahid Ari on 8/30/2016.
 */

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.wahidari.navtab.fragment.Fragment1;
import com.example.wahidari.navtab.fragment.Fragment2;
import com.example.wahidari.navtab.fragment.Fragment3;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            return new Fragment1();
        } else if (position == 1) {
            return new Fragment2();
        } else return new Fragment3();
    }

    @Override
    public int getCount() {
        return 3;
    }
}
